package bankmanagementsystem;

import java.util.ArrayList;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class BankManagementTest {

    ArrayList<BankManagement> demo;

    @BeforeMethod
    public void setUp() {
        demo = new ArrayList<>();
    }

    @Test(dataProvider = "listData", dataProviderClass = DetailsProvider.class, groups = {"sanity","full"}, dependsOnMethods = "mid")
    public void detailsAddTest(String accno, String name,
                               String phoneNumber, String emailId) {

        BankManagement obj = new BankManagement();

        obj.accno = accno;
        obj.name = name;
        obj.phoneNumber = phoneNumber;
        obj.emailId = emailId;

        demo.add(obj);

        Assert.assertEquals(obj.accno, accno);
        Assert.assertEquals(obj.name, name);
        Assert.assertEquals(obj.phoneNumber, phoneNumber);
        Assert.assertEquals(obj.emailId, emailId);
    }

    @Test(dataProvider = "managerData", dataProviderClass = DetailsProvider.class, groups = {"sanity","full"})
    public void mid(String[] m) {
        BankManagement bm =new BankManagement();
        String id = m[0];
        if (id.equalsIgnoreCase(bm.mgr_id)) {
            System.out.println("matched with " + id);
        } else {
            Assert.fail("Condition not matched for id: " + id);
        }
    }

    @Test(dataProvider = "listData1", dataProviderClass = DetailsProvider.class, dependsOnMethods = "detailsAddTest", groups = {"sanity","full"})
    public void accountHandlingTest(String ac, double bal, int yr) {

        BankManagement obj = new BankManagement();

        obj.Acc_type = ac;
        
        demo.add(obj);

        Assert.assertEquals(obj.Acc_type, ac);

        if (ac.equalsIgnoreCase(obj.Acc_type) && ac.equalsIgnoreCase("Fixed Deposit")) {
            obj.balance = bal;
            obj.years = yr;
            Assert.assertEquals(obj.years, yr);
            Assert.assertEquals(obj.balance, bal);
        } else if(ac.equalsIgnoreCase(obj.Acc_type)){
            Assert.assertEquals(obj.Acc_type, ac);
        }
        else{
            Assert.fail("account type not found");
        }
    }

    @AfterMethod
    public void show() {
        for (BankManagement bm : demo) {
            System.out.println("Account No: " + bm.accno);
            System.out.println("Account Holder Name: " + bm.name);
            System.out.println("Phone Number: " + bm.phoneNumber);
            System.out.println("Email Id: " + bm.emailId);
            System.out.println("Account_type : " + bm.Acc_type);
            System.out.println("Deposit amount is : " + bm.balance);
            System.out.println("Period (in years) : " + bm.years);
            System.out.println();
        }
    }
}
