package bankmanagementsystem;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.StringJoiner;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import bankmanagementsystem.BankManagement;
import bankmanagementsystem.Manager;
import bankmanagementsystem.customer;

public class SharatBankTest {

    private BankManagement bankManagement;
    private Manager manager;
    private customer customer;

    @BeforeClass
    public void setUp() {
        // Initialize the BankManagement, Manager, and customer instances
        bankManagement = new BankManagement();
        manager = new Manager();
        customer = new customer();
        BankManagement acc1 = new BankManagement();
        acc1.accno = "A1";
        acc1.Acc_type = "Savings Account";
        acc1.name = "Sharat";
        acc1.balance = 1000;
        acc1.emailId = "Sharat@gmail.com";
        acc1.phoneNumber = "223456";
        acc1.years = 1;
        bankManagement.account_list.add(acc1);

    }

    @Test(groups = { "sanity","full" })
    public void testDepositAmount() {
        customer.depositAmount(500.0, bankManagement.account_list.get(0));
        Assert.assertEquals(bankManagement.account_list.get(0).balance, 1500.0);
    }

    @Test(groups = { "sanity" ,"full"})
    public void testCheckBalance() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);
        customer.checkBalance(bankManagement.account_list.get(0));
        String actualString = outputStream.toString().trim();
        double actualBalance = Double.parseDouble(actualString);
        Assert.assertEquals(actualBalance, 1000.0);
    }

    @Test(groups = { "sanity" ,"full"})
    public void testWithdrawAmount() {
        customer.withdrawAmount(500.0, bankManagement.account_list.get(0));
        Assert.assertEquals(bankManagement.account_list.get(0).balance, 1000.0);
    }

    @Test(groups = { "regression" ,"full"})
    public void testWithdrawAmountNegetive() {
        customer.withdrawAmount(1500.0, bankManagement.account_list.get(0));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        System.setOut(printStream);
        customer.withdrawAmount(1500, bankManagement.account_list.get(0));
        String actualString = outputStream.toString().trim();
        Assert.assertEquals(actualString, "Entered amount is more than your account balance.");
    }

    @Test(groups = { "sanity","full" })
    public void testModifyBankAccount() {
        InputStream sysInBackup = System.in;
        String userInput = "Hegde\n1234567890\nhegde@gmail.com\n";
        ByteArrayInputStream in = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(in);
        customer c = new customer();
        c.modify_bank_account(bankManagement);
        Assert.assertEquals(bankManagement.name, "Hegde");
        Assert.assertEquals(bankManagement.phoneNumber, "1234567890");
        Assert.assertEquals(bankManagement.emailId, "hegde@gmail.com");

        System.setIn(sysInBackup);
    }

    private ByteArrayOutputStream outputStream;
    private PrintStream originalOut;

    @BeforeClass
    public void setUpStreams() {
        outputStream = new ByteArrayOutputStream();
        originalOut = System.out;
        System.setOut(new PrintStream(outputStream));
    }

    @AfterClass
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test(groups = { "sanity","full" })
    public void testInvalidAccountNumber() {
        InputStream sysInBackup = System.in;
        String userInput = "InvalidAccount\n";
        ByteArrayInputStream in = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(in);
        customer c = new customer();
        System.out.println("Dear customer, Please Enter your Account Number");
        String acc = new Scanner(System.in).next();
        BankManagement cust_ref = c.modify(acc);
        Assert.assertNull(cust_ref);
        Assert.assertEquals(outputStream.toString().trim(), "Dear customer, Please Enter your Account Number");
        System.setIn(sysInBackup);
    }

    @Test(expectedExceptions = IllegalArgumentException.class ,groups = { "regression" ,"full"})
    public void testWithdrawAmountException() {
        customer.withdrawAmount(10000000000.0, bankManagement.account_list.get(0));
    }

    @Test(expectedExceptions = IllegalArgumentException.class ,groups = { "regression" ,"full"})
    public void testDepositAmountException() {
        customer.depositAmount(-500.0, bankManagement.account_list.get(0));
    }

    @Test(expectedExceptions = IllegalArgumentException.class,groups = { "regression" ,"full"})
    public void testWithdrawAmountException2() {
        customer.withdrawAmount(-100, bankManagement.account_list.get(0));
    }

}
