package bankmanagementsystem;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;import bankmanagementsystem.Manager;
public class ManagerTest
{
    //Providing data using data provider.
    @DataProvider(name="data")
    public Object[][] data_provider()
    {

        return new Object[][]{{"123","Hemanth","saving","1234567890","hemanth@gmail.com",0.0},
    {"425","Petronas","saving","1234567890","hemanth@gmail.com",0.0}};
    }

    //Setting custom data as BAnkManagement Databaseis Empty
    @Test(dataProvider = "data")
    void add_customer_test(String accno, String name, String Acc_type,String phoneNumber,String EmailId, double balance)
    {
            BankManagement obj1= new BankManagement();
            obj1.set_acc_details(accno, name, Acc_type, phoneNumber, EmailId, balance);
            obj1.account_list.add(obj1);
            Assert.assertEquals(accno, obj1.accno);
            Assert.assertEquals(Acc_type, obj1.Acc_type);
            Assert.assertEquals(phoneNumber, obj1.phoneNumber);
            Assert.assertEquals(EmailId, obj1.emailId);
            Assert.assertEquals(balance, obj1.balance);
    }

// Testing modifyDetails methods by modifying already stored data
    @Test(priority = 2)
    void Test_modifying_method_positive()
    {
        BankManagement obj= new BankManagement();
        Manager m=new Manager();
        m.modifyDetail("123","Oneplus","saving","1234567890","hemanth@gmail.com");
    }

    @Test(priority = 2, expectedExceptions = IllegalArgumentException.class)
    void Test_modifying_method_negative()
    {
        BankManagement obj= new BankManagement();
        Manager m=new Manager();
        m.modifyDetail("12","Oneplus","saving","1234567890","hemanth@gmail.com");
    }

// Testing weather chandes done by manager reflecting in the Database.
    @Test(priority = 3)
    void Test_modified_method()
    {
        BankManagement obj= new BankManagement();
        obj=obj.modify("123");
        if(obj==null)
        {
            throw new IllegalArgumentException("Account Not Found");
        }
        else
        {
            System.out.println(obj.name);
            Assert.assertEquals(obj.name,"Oneplus");
        }
    }

}
