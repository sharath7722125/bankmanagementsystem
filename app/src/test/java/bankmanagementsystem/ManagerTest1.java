package bankmanagementsystem;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ManagerTest1 {

    @DataProvider(name = "ManagerTest1")
   public Object[][] Data() {
       return new Object[][]{
               {"Sharath",},
               {"Hemanth"},
               {"Tejas"},
               {"Sowmya"}
       };
   }

   @DataProvider(name = "ManagerTest2")
   public Object[][] Data1() {
       return new Object[][]{
               {"12345"},
               {"5678"},
            //    {"T37u7"},
               {"65757"},
               {"1234"},
            //    {"S12345678987654323456a"}
               
       };
    }
    @Test
    void testing0()
    {

        Manager obj = new Manager("asha");
        Assert.assertEquals(obj.getmodifiedString(), "asha");
    }
   @Test(dataProvider = "ManagerTest1")
    void testing1(String val)
    {

        Manager obj = new Manager(val);
        // Assert.assertEquals(obj.getmodifiedString(), "asha");
    }

    @Test
    void testing2()

    {
        Manager obj = new Manager("qwerty");
        Assert.assertEquals(obj.delete("123"), "0");

    }

    @Test(dataProvider = "ManagerTest2")
    void testing3(String val1)

    {
        Manager obj = new Manager("qwerty");

        Assert.assertEquals(obj.delete(val1), "0");
        Assert.assertEquals(obj.delete(val1), "0");
        Assert.assertEquals(obj.delete(val1), "0");
        Assert.assertEquals(obj.delete(val1), "1");




    }

    

}

/*public class BankManagement{
    @Test
    void testing1()
        {
            BankManagement obj = new BankManagement();
            obj.set_acc_details(123,"qwerty","saving","1234567890","hemanth@gmail.com",1233);
            Assert.assertEquals(obj.getmodifiedString(), "asha");
        }
    
}
int getid()*/
